using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllersWithViews();
builder.Services.AddSpaStaticFiles(c =>
{
#if(DEBUG)
    c.RootPath = "ClientApp/build";
#else
    // для publish
    c.RootPath = "wwwroot";
#endif
});

var app = builder.Build();


app.UseStaticFiles();
app.UseSpaStaticFiles();
app.UseRouting();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");


#if (!DEBUG)

// если используем SPA, то ReactDevelopmentServer перехватывает запрос к контроллеру
// Однако, если задеплоить и запустить екзешник, то почему-то работает.

app.UseSpa(spa =>
{
    spa.Options.SourcePath = "ClientApp";
    if (builder.Environment.IsDevelopment())
    {
        spa.UseReactDevelopmentServer(npmScript: "start");
        //spa.UseProxyToSpaDevelopmentServer("");
    }

});

#else

// если SPAStaticFile, то
// 1) некрасивый адрес /index.html
// 2) если обновить страницу с псевдо-путем например http://localhost:5250/fetch-data - она вдруг становится не найдена :(
// зато контроллер погоды работает
app.MapGet("/", async r =>
{
    r.Response.Redirect("/index.html");
});

#endif



app.Run();
